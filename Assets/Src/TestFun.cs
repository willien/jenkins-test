﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;

public class TestFun : MonoBehaviour {
    public float speed = 1000.0f;
    public float r = 7.0f;
    public Transform target;
    public List<GameObject> elementsList = new List<GameObject>();


    public float timeCount = 0f;


    public int angelDis = 15;
	// Use this for initialization
	void Start () {


        //setElements();
        setelements2();

      //  StartCoroutine(StartCoutine(true));
	}
	
	// Update is called once per frame
	void Update () {


        //transform.parent.RotateAroundLocal(Vector3.up, speed*Time.deltaTime);

        //foreach(GameObject obj in elementsList)
        //{
        //    obj.transform.RotateAround(target.localPosition, Vector3.up, speed * Time.deltaTime);
	
        //}
        //transform.parent.RotateAround(target.localPosition,Vector3.up, speed * Time.deltaTime);


        //transform.Rotate(Vector3.up,0.3f);
        timeCount  += Time.deltaTime;
        if(timeCount >=2f)
        {
            StartCoroutine(StartCoutine(true));
            timeCount = 0f;
        }
      
	}
    
    public IEnumerator StartCoutine(bool flag)
    {
        foreach (GameObject obj in elementsList)
        {
            yield return new WaitForSeconds(0.1f);
            float i = System.Convert.ToSingle(obj.name);
            if (flag)
            {
                i += 6 * angelDis + angelDis/2;
            }
                
            float px = r * Mathf.Cos((i * Mathf.PI) / 180f);
            float pz = r * Mathf.Sin((i * Mathf.PI) / 180f);
            obj.name = (i).ToString();
            HOTween.To(obj.transform, 0.7f, "position", new Vector3(px, 0, pz));//.OnComplete(completeCallBack,350,Vector3.one);
        }
      
    }
 
  
    public void setElements()
    {
        for (int j = 0; j < 4;j++ )
        {
            for (int i = 0; i < 16; i++)
            {
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);

                int row = i % 4;
                int col = i / 4;

                float px = col * 4f;
                float pz = row * 4f;

                float py = j * 4f;

                obj.transform.parent = transform;
                obj.transform.localPosition = new Vector3(px, py, pz);
                obj.name = "Sphere" + i+ "_"+j;
                obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                elementsList.Add(obj);
            }
        }

        transform.localPosition += new Vector3(-6f, -6f, -6f);
    }


    public void setelements2()
    {

        for (int i = 0; i < 360; i += angelDis)
        {
           
            float px = r * Mathf.Cos((i*Mathf.PI)/180f);
            float pz = r * Mathf.Sin((i * Mathf.PI) / 180f);

           
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            obj.transform.parent = transform;
            obj.transform.localPosition = new Vector3(px, 0, pz);
            obj.name =  i.ToString();
 
            if ((i / angelDis) % 3 == 0)
                obj.GetComponent<Renderer>().material.color = Color.red;
            else if ((i / angelDis) % 3 == 1)
                obj.GetComponent<Renderer>().material.color = Color.yellow;
            else
                obj.GetComponent<Renderer>().material.color = new Color(0f, 181f, 255f);
            elementsList.Add(obj);
            //Debug.Log("angle.." + i + "  pos:"+obj.transform.localPosition);
        }
    }
}
